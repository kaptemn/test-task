# Test Task

## Demo 

Here is a live demo page and the repo for it:
[https://lionfish-app-ia97k.ondigitalocean.app/](https://lionfish-app-ia97k.ondigitalocean.app/)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```
