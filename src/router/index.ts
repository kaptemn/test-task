import { createRouter, createWebHistory } from 'vue-router'
import PlaceZipcode from '../views/PlaceZipcode.vue'
import IPLookup from '../views/IPLookup.vue'
import HttpData from '../views/HttpData.vue'
import Home from '../views/Home.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      component: Home
    }, 
    {
      path: '/http-data',
      component: HttpData
    }, 
    {
      path: '/zipcode',
      component: PlaceZipcode
    }, 
    {
      path: '/ip-lookup',
      component: IPLookup
    },
  ]
})

export default router
