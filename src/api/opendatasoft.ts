import axios from 'axios';

const OpendataSoftClient = axios.create({
  baseURL: 'https://data.opendatasoft.com',
  timeout: 1000,
});

export {
  OpendataSoftClient
} 