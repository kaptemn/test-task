import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/main.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faLocationDot, faGlobe, faMagnifyingGlass, faFile, faInfo } from '@fortawesome/free-solid-svg-icons'

import Notifications from '@kyvg/vue3-notification'

library.add(faLocationDot, faGlobe, faMagnifyingGlass, faFile, faInfo)

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(Notifications, {
  position: 'center'
})
app.component('font-awesome-icon', FontAwesomeIcon)


app.mount('#app')
